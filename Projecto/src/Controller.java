import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import java.io.File;
import java.io.IOException;

public class Controller {

    static File selectedFile;
    public BufferedReader reader = null;
    private void showErrorDialog(String msg)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setContentText(msg);
        alert.showAndWait();
    }
    private boolean openFile() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Text File");
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.jpg","*.png",".txt", ".csv","*.txt", "*.TeloEncodeo"));
        selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile == null)
        {
            String fileName = "Fox.txt";
            try
            {
                reader = new BufferedReader(new FileReader("data/"+fileName));
            }
            catch (IOException e)
            { return false;
            }

        }

        else
        {
            try
            {
                reader = new BufferedReader(new FileReader(selectedFile));

                //    buttonReadLine.setDisable(false);
            }
            catch (IOException e)
            { showErrorDialog("IO Exception: " + e.getMessage());
                return false;
            }           // labelFileName.setText(selectedFile.getName());

        }

        return true;
    }
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button EncyptButton;
    @FXML
    private Button DecryptButton;
    @FXML
    void PressEncryptButton(ActionEvent event) throws IOException {
        Object source = event.getSource();

        if (source ==EncyptButton) openFile();

        if(reader != null){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Ventana2.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("My New Stage Title");
            stage.setScene(new Scene(root, 735, 510));
            stage.showAndWait();


        }

    }
    @FXML
    void PressDecryptButton(ActionEvent event) throws IOException {

        Object source = event.getSource();

        if (source ==DecryptButton) openFile();

        if(reader != null){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Ventana3.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("My New Stage Title");
            stage.setScene(new Scene(root, 735, 510));
            stage.showAndWait();

        }


    }
    @FXML
    void initialize() {
        assert EncyptButton != null : "fx:id=\"EncyptButton\" was not injected: check your FXML file 'sample.fxml'.";
        assert DecryptButton != null : "fx:id=\"DecryptButton\" was not injected: check your FXML file 'sample.fxml'.";
    }
}
