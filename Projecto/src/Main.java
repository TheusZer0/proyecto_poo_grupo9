import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {
    static String Key;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("TELoencodeo");
        primaryStage.setScene(new Scene(root, 735, 510));
        primaryStage.show();
    }


    public static void main(String[] args) throws IOException {


        launch(args);
    }
}
