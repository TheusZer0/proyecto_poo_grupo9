import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;


public class Controller3 {

    @FXML
    private Label LabelFIleDescrypt;

    @FXML
    private Button ButtonParaText;

    @FXML
    private PasswordField decryptKey;

    @FXML
    private Label labelKEY;

    @FXML
    void ButtonParaTextAction(ActionEvent event) throws FileNotFoundException {
        String input = decryptKey.getText();
        //intento para crear el nuevo archivo pero no funciona bien
        //File salida =new File(Controller.selectedFile.getParent() ,"/hol.txt");
        //System.out.println(salida.getAbsolutePath());
        File out_hex = new File(Controller.selectedFile.getAbsolutePath()); //archivo que esta encryptado en AES
        File out_img = new File(Controller.selectedFile.getName().replace(".TeloEncodeo","")); //archivo desencriptado de hex a file

        String message = input;
        if (message!=null && Main.Key!=null) {
            if (Main.Key.equals(message)) {
                labelKEY.setText("Key correcta");
                OutputStream os_two = new FileOutputStream(out_img); //archivo de salida que ya fue desencriptado de hex a archivi
                Encrypt_and_Decrypt.decrypt(out_hex);
                Hex_Functions.hexToBinary(out_hex,os_two); //no lo hace debido a que le estoy pasando el archivo que esta encodeado
                out_hex.delete();
                Stage stage = (Stage) ButtonParaText.getScene().getWindow();
                stage.close();   // Para cerrar la ventana luego de apretar el boton
            }
            else{
                labelKEY.setText("Key incorrecta, intente de nuevo");
            }
        }

    }
    @FXML
    void initialize() {
        assert ButtonParaText != null : "fx:id=\"ButtonParaText\" was not injected: check your FXML file 'Ventana2.fxml'.";
        //assert labelFIle != null : "fx:id=\"labelFIle\" was not injected: check your FXML file 'Ventana2.fxml'.";
        LabelFIleDescrypt.setText(Controller.selectedFile.getName());
    }
}