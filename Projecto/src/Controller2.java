import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

public class Controller2 {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button ButtonParaText;

    @FXML
    private PasswordField encryptKey;

    @FXML
    public Label labelFIle;

    @FXML
    void ButtonParatextAction(ActionEvent event) throws IOException {
        String input = encryptKey.getText();
        Main.Key = input;
        //aqui llamar al metodo encrypt con la key y el archivo utilizando Controller.selectedFile.getAbsolutePath();
        //para crear guardar el archivo nuevo en el mismo directorio ocupar Controller.selectedFile.getParent()
        String tmp_string_hex = Hex_Functions.file_to_hex(Controller.selectedFile); //se pasa el archivo a hex
        Encrypt_and_Decrypt.encrypt(tmp_string_hex,Controller.selectedFile); //se realiza el encrypt del archivo hex
        Stage stage = (Stage) ButtonParaText.getScene().getWindow();
        stage.close();   // Para cerrar la ventana luego de apretar el boton
    }

    @FXML
    void initialize() {
        assert ButtonParaText != null : "fx:id=\"ButtonParaText\" was not injected: check your FXML file 'Ventana2.fxml'.";
        //assert labelFIle != null : "fx:id=\"labelFIle\" was not injected: check your FXML file 'Ventana2.fxml'.";
        assert encryptKey != null : "fx:id=\"encryptKey\" was not injected: check your FXML file 'Ventana2.fxml'.";
        labelFIle.setText(Controller.selectedFile.getName());
    }
}
