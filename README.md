## Proyecto_POO_Grupo9

> **Integrantes**:
>
>> Alondra Araya
>
>> Robert Parra
>
>> Felipe Vega
>
>> Felipe Bustos

## TeloEncodeo Software

#### Codigo

El codigo correspondiente se encuentra en el directorio "Projecto/src" del mismo proyecto.

#### Run

Para realizar el correcto funcionamiento del codigo se debe tener descargado e instalado
las funcionalidades de las librerias de javaFX.

Una vez dejado esto en claro, tenemos que entrar a la carpeta "Projecto/src" y abrir con su editor de texto preferido el archivo de nombre **Makefile**.

> En el archivo Makefile se debe cambiar la primera linea por el path en donde se encuentra tus librerias de JavaFX:
>> ```JFXLIB = "/home/theuszero/Programs/javafx-sdk-11.0.2/lib" #change this for your JavaFx Lib```

> Una vez ya cambiado, se debe de estar en el directorio **"Projecto/src"** y ejecutar el siguiente comando:
>> ```make```
>
> Este generara las clases *.class de java.

> Para ejecutar el programa debemos usar:
>> ```make run```

> Para limpiar los *.class generados:
>> ```make clean```

## Pagina HTML - TeloEncodeo

la pagina HTML se encuentra en el directorio **"WebPage/PROYECTO_POO"** y para usar la pagina html basta con abrir el index.html que se encuentra en la carpeta **"WebPage/PROYECTO_POO"**